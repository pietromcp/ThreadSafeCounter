﻿namespace ThreadSafeCounter {
    public class Counter {
        public Counter(int initialValue) {
            Value = initialValue;
        }

        public Counter(): this(0) {            
        }

        public int Value { get; private set; }

        public void Increment() {
            lock (this) {
                Value++;
            }
        }
    }
}